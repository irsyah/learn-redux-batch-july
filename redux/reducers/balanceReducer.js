
// reducer sebuah function biasa yang menerima 2 parameter: state dan action
// action adalah object yang memiliki key type dan payload
// { type: '', payload: '' }
// reducer itu harus mereturn state
const balanceReducer = (state = 1000, action) => {
    switch(action.type) {
        case 'DEPOSIT':
            return state + action.payload;
            break;
        case 'WITHDRAW':
            return state - action.payload;
            break;
        default:
            return state;
            break;
    }
}

export default balanceReducer;