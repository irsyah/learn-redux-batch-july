import { combineReducers } from 'redux'
import balanceReducer from './balanceReducer'
import todoReducer from './todoReducer'
import userProfileReducer from './userProfileReducer'

const reducers = combineReducers({
   balance: balanceReducer,
   userProfile: userProfileReducer,
   todos: todoReducer
})

export default reducers