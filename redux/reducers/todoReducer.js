const todoReducer = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_TODO':
            return action.payload
            break;
        case 'REMOVE_TODO':
            return state.filter((todo) => {
                if (todo.id !== action.payload) {
                    return todo;
                }
            })
            break;
        case 'ADD_TODO':
            return [ ...state, action.payload]
            break;
        default:
            return state;
            break;
    }
}

export default todoReducer