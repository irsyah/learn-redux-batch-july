import { View, Text, SafeAreaView, TextInput, Button } from 'react-native'
import React, { useState } from 'react'

import { useDispatch } from 'react-redux';

export default function Deposit() {
  const [ depositMoney, setDepositMoney ] = useState(0);

//   const balance = useSelector((state) => state)// untuk mengambil state pada store
  const dispatch = useDispatch();

  const deposit = () => {
    //dispatch
    dispatch({type: 'DEPOSIT', payload: +depositMoney})
    setDepositMoney(0)
  }

  return (
    <SafeAreaView>
      <Text>COMPONENT DEPOSIT</Text>
      <TextInput value={depositMoney} onChangeText={setDepositMoney}/>
      <Button title="Deposit" onPress={deposit}/>
    </SafeAreaView>
  )
}