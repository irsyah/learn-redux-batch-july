import { View, Text, SafeAreaView } from 'react-native'
import React from 'react'

import { useSelector } from 'react-redux'
import Deposit from './Deposit';
import Withdraw from './Withdraw';
import TodoScreen from './TodoScreen';

export default function Home() {
  const balance = useSelector((state) => state.balance);
  const userProfile = useSelector((state) => state.userProfile)

  return (
    <SafeAreaView>
      <Text>Deposit: {balance} </Text>
      <Text>{userProfile.username} berumur {userProfile.age}</Text>
      {/* <Deposit />
      <Withdraw /> */}
      <TodoScreen />
    </SafeAreaView>
  )
}