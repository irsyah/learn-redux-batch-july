import { View, Text, FlatList, Alert, TouchableOpacity } from 'react-native'
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

export default function TodoScreen() {
  const todos = useSelector((state) => state.todos);
  const dispatch = useDispatch();

  const fetchTodo = async () => {
    try {
        let response = await fetch('https://jsonplaceholder.typicode.com/todos');
        if (response.ok) {
            let data = await response.json();
            // setTodos(data); // SUDAH TIDAK PAKE SET DARI USESTATE KARENA STATE SUDAH DI STORE
            // pokoknya kalo mau ubah data state di store itu pasti pake dispatch yang mengirimkan action
            dispatch({ type: 'FETCH_TODO', payload: data })
        }
    } catch(err) {
        Alert.alert("Error Fetch Todo")
    }
  }

  const removeTodo = async (id) => {
    try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
            method: 'DELETE'
        });
        if (response.ok) {
           let dataDelete = await response.json();
           dispatch({ type: 'REMOVE_TODO', payload: id })
        }
    } catch(err) {
        Alert.alert("Error Remove Todo")
    }
  }

  const addTodo = async () => {
    try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/todos`, {
            method: 'POST',
            body: JSON.stringify({
                title: 'Learn Redux',
                completed: true,
                userId: 4

            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        });
        if (response.ok) {
           let dataAdded = await response.json();
           dispatch({ type: 'ADD_TODO', payload:  dataAdded })
        }
    } catch(err) {
        Alert.alert("Error Remove Todo")
    } 
  }

  useEffect(() => {
    fetchTodo()
  }, [])

  return (
    <View>
      <TouchableOpacity onPress={addTodo}>
        <Text>ADD HARD CODE</Text>
      </TouchableOpacity>
      <FlatList 
        data={todos}
        renderItem={({item}) => (
            <View>
                <Text>{item.title}</Text>
                <TouchableOpacity onPress={() => removeTodo(item.id)}>
                    <Text>Delete</Text>
                </TouchableOpacity>
            </View>
        )}
      />
    </View>
  )
}