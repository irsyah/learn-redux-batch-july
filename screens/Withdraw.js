import { View, Text, SafeAreaView, TextInput, Button } from 'react-native'
import React, { useState } from 'react'

import { useDispatch } from 'react-redux';

export default function Withdraw() {
//   const balance = useSelector((state) => state);
  const dispatch = useDispatch();

  const [ withdrawMoney, setWithdrawMoney ] = useState(0);

  const withdraw = () => {
    dispatch({ type: 'WITHDRAW', payload: +withdrawMoney});
    setWithdrawMoney(0)
  }

  return (
    <View>
      <Text>COMPONENT WITHDRAW</Text>
      {/* <Text>balance: {balance} </Text> */}
      <TextInput value={withdrawMoney} onChangeText={setWithdrawMoney}/>
      <Button title="Withdraw" onPress={withdraw}/>
    </View>
  )
}