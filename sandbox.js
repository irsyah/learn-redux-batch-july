let user = { username: 'Icha', age: 3 };

let userCopy = { ...user }; //
userCopy.age = 10; // saya ubah data age untuk userCopy jadi 10
console.log(userCopy);

console.log(user)
// let userCopy = { ...user, age: user.age+1 }
// console.log(userCopy)

// let numbers = [ 3, 2, 4 ];

// let angka = [ ...numbers ];

// console.log(angka);

// KENAPA DI REACT JS KETIKA MENGUBAH DATA OBJECT ATAUPUN ARRAY ITU HARUS MENGGUNAKAN SPREAD SYNTAX ATAU DI COPY

