# DOKUMENTASI SERVER
- URL:
    - http_method: GET
    - `http://movie-app-g2.herokuapp.com/vehicles`
  - Expected response (status: `200`):
    ```json
      [{
        "_id": "...",
        "type_of_vehicle": "...",
        "image": "...",
        "price": "...",
        "status": true/false 
      }]
    ```

- URL:
    - http_method: PATCH
    - `http://movie-app-g2.herokuapp.com/vehicles/:id`
    - body: 
        ```json
        { "status": "..." }
        ```
  - Expected response (status: `200`):
    ```json
      {
        "message": "success to updated" 
      }
    ```