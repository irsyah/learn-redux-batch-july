# VEHICLE RENTAL APP

Designlah app sederhana menggunakan react component

- Dokumen SERVER API silakan dilihat di file server.md

Requirement:

- pada store buatlah state "user" dengan default value sebagai berikut:
 ```json
 {
     "user" : "<nama_kamu>",
     "balance: 5000
 }
 ```

- Tampilan User dan balance harus berada di Component Navbar/Head
    - Hi "<user>" your balance: "<balance>
    - convert balance menjadi currency dollar (**HINT: keyword bisa dicari dengan "number to currency format in js"**)

- Pada component vehicle terdapat tombol "Rent", saat tombol tersebut di-klik maka:
    - akan hit API untuk meng-edit status vehicle tersebut menjadi true
    - balance user akan berkurang sesuai dengan harga vehicle yang disewa

- Tombol "Rent" hanya muncul saat status vehicle bernilai false

- Jika balance tidak cukup untuk menyewa Vehicle, tombol "Rent" harus "disabled"

- Pada Component vehicle terdapat tombol "Return", saat tombol tersebut di-klik maka:
    - akan hit API untuk meng-edit status vehicle tersebut menjadi false

- Tombol "Return" hanya muncul saat status vehicle bernilai true

- Saat tombol Rent ataupun Return di click, tampilan di UI harus reactive (balance juga langsung berubah)

# CARA PENGUMPULAN
Buatlah private repo dengan nama react-native-car-rent-namaKamu, add irsyah dengan role developer dengan akses sampai 31 Desember 2022

# SCORE

- Component rapi dan re-usable (**20 points**)
- Data Flow sesuai dengan aturan parent, child dan siblings, Redux (**35 points**)
- Tombol Rent maupun Return berfungsi (**20 points**)
- Reactive (**15 points**)
- Tampilan (**10 points**)